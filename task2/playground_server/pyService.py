import subprocess
import traceback

from flask import Flask, request
import json
from flask import Response

app = Flask(__name__, static_url_path='')

@app.route("/py/eval", methods=['GET', 'POST'])
def handle():
    if request.method == 'POST':

        # Implementation goes here.
        #
        # Both stdout and stderr should be captured.
        # {"stdout": "<output_from_stdout>", "stderr": "<output_from_stderr>"}
        data = {'stdout': "",
                'stderr': ""}
        try:
            dictt = json.loads(request.data)
            code = dictt["code"]
            pipes = subprocess.Popen(["python3", "-c", code],
                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            std_out, std_err = pipes.communicate()

            data['stdout'] = std_out.decode("utf-8")
            data['stderr'] = std_err.decode("utf-8")
        except:
            pass

        return Response(json.dumps(data), mimetype='application/json')

if __name__ == '__main__':
    app.run(threaded=True, debug=True, host="0.0.0.0", port=6000)
